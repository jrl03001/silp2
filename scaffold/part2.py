#!/usr/bin/python
'''
creates node in multi scaffold graph
'''

### imports ###
import sys
import os
import logging
import networkx as nx

import helpers.io as io
import optimize.order as order

### definitions ###


### functions ###

def run_ordering(paths, args):
    """ runs orientation
    Parameters
    ----------
    paths.orient_file       : file
    paths.order_file       : file
    """

    # load the oriented graph.
    DG = nx.read_gpickle(paths.orient_file)

    # check it.
    for n in DG.nodes():
        if DG.node[n]['orien'] == -1:
            logging.erorr("orientatio not set")

    for p,q in DG.edges():
        if DG[p][q]['state'] == -1:
            logging.error("state not set")

    # solve the ILP.
    ILP = order.OrderIlp("log.txt", "err.txt")
    ILP.load("card", DG)

    SG = ILP.solve()

    # ensure node degree is low.
    deg_list = [len(SG.neighbors(x)) for x in SG.nodes()]
    if max(deg_list) > 2:
        logging.error("is not a path")
        sys.exit(1)

    # remove cycles.
    for cycle in nx.simple_cycles(SG):
        print "didn't finish cycle code"
        print cycles
        sys.exit(1)

    # ensure its a DAG.
    if nx.is_directed_acyclic_graph(SG) == False:
        logging.error("not a DAG?")
        sys.exit(1)


    # write to disk.
    nx.write_gpickle(SG, paths.order_file)
