#!/usr/bin/python
'''
creates node in multi scaffold graph
'''

### imports ###
import sys
import os
import logging
import networkx as nx
import numpy as np

import helpers.io as io
import optimize.orient as orient
import helpers.graphs as graphs
import helpers.misc as misc

from pygraphviz import *

### definitions ###

DECOMP_BOUND = 50

### internal functions ###

def update_dists(G):
    ''' fixes distances once an orientation has been set '''
    # update distances.
    bad = 0
    to_remove = list()
    for p, q in G.edges():

        # get state and dist.
        state = G[p][q]['state']
        dist = G[p][q]['means'][state]

        # check if no dist.
        if dist == -1:
            to_remove.append((p,q))

            # re-estimate.
            width1 = G.node[p]['width']
            width2 = G.node[p]['width']
            ins_size = G[p][q]['ins_size']
            std_dev = G[p][q]['std_dev']

            # loop over distances.
            dists = np.zeros(len(G[p][q]['poses1']), dtype=np.int)
            i = 0
            for p1, p2 in zip(G[p][q]['poses1'], G[p][q]['poses2']):
                dists[i] = misc.determine_dist(p, q, state, width1, width2, p1[0], p1[1], p2[0], p2[1], ins_size)
            avg_dist = np.mean(dists)

            # remove if mean is too great.
            if avg_dist < (-1 * (ins_size + (6 * std_dev))):
                to_remove.append((p,q))
            else:
                G[p][q]['means'][state] = avg_dist

    # remove bad ones.
    G.remove_edges_from(to_remove)

    # return it.
    return G

def decomp_solve(BG, RG, ILP):
    ''' solves ILP using decomposition '''

    # loop over each component.
    for n in RG.nodes():

        # simplify.
        comp = RG.node[n]['comp']
        curg = RG.node[n]['graph']

        # create the applicable subgraph.
        subg = BG.subgraph(comp)

        # base case this.
        if len(comp) <= DECOMP_BOUND or curg == None:
            ILP.load(BG)
            ILP.solve(subg)

        else:

            print len(comp)
            sys.exit()


### external functions ###
def run_orientation(paths, args):
    """ runs orientation
    Parameters
    ----------
    paths.bundle_file       : file
    paths.decomp_file       : file
    """

    # load the bundle graph.
    BG = nx.read_gpickle(paths.bundle_file)
    RG = nx.read_gpickle(paths.decomp_file)

    # sanity check self edges.
    for p, q in BG.edges():
        if p == q:
            #logging.error("self edge")
            #sys.exit(1)
            BG.remove_edge(p, q)

    # annotate graph before solving.
    for p, q in BG.edges():
        BG.node[p]['orien'] = -1
        BG.node[q]['orien'] = -1
        BG[p][q]['state'] = -1


    # solve the ILP using decomposition.
    ILP = orient.OrientIlp("log.txt", "err.txt", "prg.txt", "sol.txt")
    #SG = decomp_solve(BG, RG, ILP)
    #'''
    ILP.load(BG)
    ILP.solve()
    #'''

    # check it.
    for n in BG.nodes():
        if BG.node[n]['orien'] == -1:
            logging.error("orientatio not set")

    for p,q in BG.edges():
        if BG[p][q]['state'] == -1:
            logging.error("state not set")

    # compute the directed graph.
    DG = graphs.to_directed(BG)

    '''
    # transitive reduction.
    TG = AGraph(directed=True)
    TG.add_edges_from(DG.edges())
    TG.tred()

    to_remove = list()
    for p,q in BG.edges():
        if TG.has_edge(p,q) == False:
            to_remove.append((p,q))
    DG.remove_edges_from(to_remove)
    '''
    # write to disk.
    nx.write_gpickle(DG, paths.orient_file)

