#!/usr/bin/python
'''
creates node in multi scaffold graph
'''

### imports ###
import sys
import os
import logging
import networkx as nx
import numpy as np
import subprocess

import helpers.io as io
import optimize.orient as orient
import helpers.graphs as graphs
import helpers.misc as misc

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )

### definitions ###

DECOMP_BOUND = 5
EXT_OUT = open("/dev/null")

### functions ###

def test_bi():
    ''' creates a bi-connected component '''

    # make a biconnected component.
    G = nx.Graph()
    G.add_edge(0,1)
    G.add_edge(0,3)
    G.add_edge(0,2)
    G.add_edge(1,3)
    G.add_edge(3,2)
    G.add_edge(2,4)
    G.add_edge(4,5)
    G.add_edge(5,2)
    G.add_edge(3,7)
    G.add_edge(6,7)
    G.add_edge(6,8)
    G.add_edge(9,8)
    G.add_edge(9,7)
    return G

def test_tri():
    ''' creates a tri-connected component '''

    # make a biconnected component.
    G = nx.Graph()

    G.add_edge(1,2)
    G.add_edge(1,3)
    G.add_edge(2,4)
    G.add_edge(3,4)
    G.add_edge(5,4)
    G.add_edge(5,6)
    G.add_edge(5,7)
    G.add_edge(6,8)
    G.add_edge(7,8)
    G.add_edge(2,6)
    G.add_edge(3,7)

    G.add_edge(10,11)
    G.add_edge(10,12)
    G.add_edge(13,12)
    G.add_edge(13,11)
    G.add_edge(13,14)
    G.add_edge(15,14)
    G.add_edge(16,14)
    G.add_edge(16,17)
    G.add_edge(15,17)
    G.add_edge(11,15)
    G.add_edge(12,16)

    G.add_edge(18,19)
    G.add_edge(18,20)
    G.add_edge(21,20)
    G.add_edge(21,19)
    G.add_edge(21,22)
    G.add_edge(23,22)
    G.add_edge(24,22)
    G.add_edge(24,25)
    G.add_edge(23,25)
    G.add_edge(23,19)
    G.add_edge(20,24)

    G.add_edge(0,1)
    G.add_edge(0,10)
    G.add_edge(0,18)

    G.add_edge(9,8)
    G.add_edge(9,17)
    G.add_edge(9,25)

    return G

def make_subg(G, active):
    ''' returns subgraph '''

    # make a list of it.
    comp = list(active)

    # make new graph.
    NG = nx.Graph()

    # add nodes.
    nluf = dict()
    nlur = dict()
    for i in range(len(comp)):
        NG.add_node(i)
        nluf[comp[i]] = i
        nlur[i] = comp[i]

    # add edges.
    for p, q in G.edges():

        # skip if both not active.
        if p not in active or q not in active:
            continue

        # add the edge.
        NG.add_edge(nluf[p], nluf[q])

    # return it.
    return NG, nluf, nlur

def load_decomp(in_file, nlu, prefix):
    ''' load decomposition results '''

    # load the data.
    fin = open(in_file, "rb")
    lines = fin.readlines()
    fin.close()

    # build directed graph.
    DG = nx.DiGraph()

    # tokenize once.
    tokens = [line.strip().split() for line in lines]

    # load the nodes.
    for token in tokens:
        if token[0] != "N": continue

        # build set of ids.
        comp = frozenset([nlu[int(x)] for x in token[2::]])

        # create index of this comp.
        idx = "%s_%i" % (prefix, int(token[1]))

        # add info.
        DG.add_node(idx, comp=comp, graph=None)

    # add the directed edges.
    for token in tokens:
        if token[0] != "E": continue

        # get ids.
        s = "%s_%i" % (prefix, int(token[1]))
        t = "%s_%i" % (prefix, int(token[2]))
        cut = frozenset([nlu[int(x)] for x in token[3::]])

        # add the edge.
        DG.add_edge(s, t, cut=cut)

    # return the decomposition.
    return DG

def write_graph(G, out_file):
    ''' writes graph in simple format'''

    with open(out_file, "wb") as fout:

        # write the number of nodes and edges.
        fout.write("%i\t%i\n" % (G.number_of_nodes(), G.number_of_edges()))

        # write edges.
        for p, q in G.edges():
            fout.write("%i\t%i\n" % (p, q))

def decomp0(G, tmp1_file, tmp2_file):
    ''' returns connected components '''

    # do decomposition.
    comps = nx.connected_components(G)

    # create the decomposition graph.
    DC = nx.DiGraph()

    # loop over connected components.
    idx = 0
    for comp in comps:

        # freeze the components.
        comp = frozenset(comp)

        # compute further decomp if necessary.
        if len(comp) > DECOMP_BOUND:
            dg = decomp1(G, comp, tmp1_file, tmp2_file)
        else:
            dg = None

        # add node to DC.
        DC.add_node("con_%i" % idx, comp=frozenset(comp), graph=dg)
        idx += 1

    # return the graph.
    return DC

def decomp1(G, comp, tmp1_file, tmp2_file):
    ''' bi-connected decomposition '''

    # create active subgraph.
    subg, nluf, nlur = make_subg(G, comp)

    # serialize this to disk.
    write_graph(subg, tmp1_file)

    # execute decomposition.
    cmd = ["/home/jrl03001/code/SILP2/bin/decomp0", tmp1_file, tmp2_file]
    if subprocess.call(cmd, stdout=EXT_OUT) != 0:
        logging.error("error in biconnected decomposition")
        sys.exit(1)

    # create decomposition graph.
    DC = load_decomp(tmp2_file, nlur, "bicon")

    # loop over each node in DC.
    for n in DC.nodes():

        # grab frozen component.
        comp = DC.node[n]['comp']

        # compute further decomp if necessary.
        if len(comp) > DECOMP_BOUND:
            dg = decomp2(G, comp, tmp1_file, tmp2_file)
        else:
            dg = None

        # modify node in DC.
        DC.node[n]['graph'] = dg

    # return the graph.
    return DC

def decomp2(G, comp, tmp1_file, tmp2_file):
    ''' tri-connected decomposition '''

    # create active subgraph.
    subg, nluf, nlur = make_subg(G, comp)

    # serialize this to disk.
    write_graph(subg, tmp1_file)

    # execute decomposition.
    cmd = ["/home/jrl03001/code/SILP2/bin/decomp1", tmp1_file, tmp2_file]
    if subprocess.call(cmd, stdout=EXT_OUT) != 0:
        logging.error("error in triconnected decomposition")
        sys.exit(1)

    # create decomposition graph.
    DC = load_decomp(tmp2_file, nlur, "tricon")

    # inform us of the largest component size.
    largest = -1
    for n in DC.nodes():
        if len(DC.node[n]['comp']) > largest:
            largest = len(DC.node[n]['comp'])
    logging.info("largest component: %d" % largest)

    # return the graph.
    return DC

def decompose(paths, args):
    """ runs decomposition
    Parameters
    ----------
    paths.bundle_file       : file
    paths.tmp1_file         : file
    paths.tmp2_file         : file
    paths.decomp_file       : file
    """

    # load the bundle graph.
    logging.info("loading info")
    BG = nx.read_gpickle(paths.bundle_file)
    #BG = test_bi()
    #BG = test_tri()

    # loop over each connected component.
    DC = decomp0(BG, paths.tmp1_file, paths.tmp2_file)

    # remove temp files.
    if os.path.isfile(paths.tmp1_file) == True:
        subprocess.call(["rm","-f",paths.tmp1_file])
    if os.path.isfile(paths.tmp2_file) == True:
        subprocess.call(["rm","-f",paths.tmp2_file])

    # write to disk.
    nx.write_gpickle(DC, paths.decomp_file)
